# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone git@bitbucket.org:mb3926/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/mb3926/stroboskop/commits/f03fc8bf48a630a11b4911526e6f5d340db79011

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/mb3926/stroboskop/commits/02dd017045ab9b4a964e44113d2a5d1a23d5b0dd

Naloga 6.3.2:
https://bitbucket.org/mb3926/stroboskop/commits/d09fe20c3a46ddab488b2c77df1769cc5b0b68dc

Naloga 6.3.3:
https://bitbucket.org/mb3926/stroboskop/commits/8120cfe71c541c2d4a1929e21dc319ca074af7c8

Naloga 6.3.4:
https://bitbucket.org/mb3926/stroboskop/commits/930846dcb5a6f7badcfa9a781d8af1d4178d88c0

Naloga 6.3.5:

git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/mb3926/stroboskop/commits/18e964b31a833420fc9b985bd3acaed013f697a7

Naloga 6.4.2:
https://bitbucket.org/mb3926/stroboskop/commits/53698235254a5c5ddff32496e677e00dcb91a37a

Naloga 6.4.3:
https://bitbucket.org/mb3926/stroboskop/commits/ad47b9763c4eaa26189862c3467b7abea237d350

Naloga 6.4.4:
https://bitbucket.org/mb3926/stroboskop/commits/a39d03e7b31f0869b274cdc20451f5a3e3d3fe4a